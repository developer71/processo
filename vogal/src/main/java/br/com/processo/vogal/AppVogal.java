package br.com.processo.vogal;

/**
 * @author Vagner
 *
 */
public class AppVogal {

	public static Character v[] = {'a', 'e', 'i', 'o', 'u'};
	
	public static char firstChar(Stream stream){
		int qtd[] = {0, 0, 0, 0, 0};
		String str = "";
		while(stream.hasNext()){
			char c = stream.getNext();
			str = str.length() >= 3 ? (str.substring(1, 3) + c) : (str + c);
			if(String.valueOf(c).toLowerCase().matches("[aeiou]")){
				
				for(int i = 0; i < v.length; i++){
					qtd[i] = Character.toLowerCase(c) == v[i] ? ++qtd[i] : qtd[i];
					if(qtd[i] == 1 && v[i] == Character.toLowerCase(c) 
							&& str.toLowerCase().matches("\\w*[aeiou][^aeiou][aeiou]\\w*"))
						return c;
				}	
			}	
		}		
		return 0;
	}
	
	public static void main(String[] args) {
		
	}

}
