package br.com.processo.vogal;

/**
 * @author Vagner
 *
 */
public class Vogal implements Stream{

	private char[] arrayVogais;
	private int contVogal = 0;
	
	public Vogal(String palavra){
		this.arrayVogais = palavra.toCharArray();
	}
	
	@Override
	public boolean hasNext(){
		return contVogal < arrayVogais.length;
	}
	
	@Override 
	public char getNext(){
		return arrayVogais[contVogal++];
	}
}
