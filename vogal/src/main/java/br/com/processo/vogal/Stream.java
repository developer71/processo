package br.com.processo.vogal;

/**
 * @author Vagner
 *
 */
public interface Stream {
	boolean hasNext();
	char getNext();
}
