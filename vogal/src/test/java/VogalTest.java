import org.junit.Assert;
import org.junit.Test;

import br.com.processo.vogal.AppVogal;
import br.com.processo.vogal.Vogal;

/**
 * @author Vagner
 *
 */
public class VogalTest {

	
	@Test
	public void testVogais(){
		Assert.assertEquals(AppVogal.firstChar(new Vogal("Abe")), 'e');
		Assert.assertEquals(AppVogal.firstChar(new Vogal("AabicedjIk")), 'i');
		Assert.assertEquals(AppVogal.firstChar(new Vogal("AiabiceedujIk")), 'e');
		Assert.assertEquals(AppVogal.firstChar(new Vogal("eAiabiceedUjIkxzYvlp")), 'U');
		Assert.assertEquals(AppVogal.firstChar(new Vogal("")), 0);
	}
	
}
