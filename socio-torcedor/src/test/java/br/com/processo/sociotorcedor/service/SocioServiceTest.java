package br.com.processo.sociotorcedor.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.processo.sociotorcedor.entity.Socio;
import br.com.processo.sociotorcedor.repository.SocioRepository;

@RunWith(MockitoJUnitRunner.class)
public class SocioServiceTest {
	
	@Mock
	private SocioRepository socioRepository;
	
	@InjectMocks
	private SocioService socioService = new SocioServiceImpl();
	
	private Optional<Socio>  criarSocioOk(){
		
		Socio socio = new Socio();
		socio.setId(1L);
		socio.setNome("Vagner");
		socio.setEmail("develoer71@gmail.com");
		socio.setIdTimeCoracao(1l);
		DateTimeFormatter formatadorBarra = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String format = "1990-10-12";
		socio.setDataNascimento(LocalDate.parse(format));
		Optional<Socio> optional = Optional.ofNullable(socio);
		return optional;
	}
	   
	@Test
	public void testFindById() {
		
		Socio socio = criarSocioOk().get();
		when(socioRepository.findOne(eq(socio.getId()))).thenReturn(socio);
		Optional<Socio> optional = socioService.findOne(1l);
		Assert.assertEquals(optional.get().getId(), socio.getId());
		
	}


	@Test
	public void testFindByEmail() {
		Optional<Socio> optional = criarSocioOk();
		when(socioRepository.findByEmail(optional.get().getEmail())).thenReturn(optional);
		
		Optional<Socio> opc = socioService.findOne(1l);
		assertTrue(optional.get().getEmail() != null);
				
	}

	@Test
	public void testSaveOk() {
		Socio socio = criarSocioOk().get();
		when(socioRepository.save(socio)).thenReturn(socio);
		assertTrue(socio != null);
	}
	
	@Test
	public void testSaveComFalha() {
		Socio socio = null;
		when(socioRepository.save(socio)).thenReturn(null);
		assertTrue(socio == null);
		
	}

}
