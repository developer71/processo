/**
 * 
 */
package br.com.processo.sociotorcedor.client;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Vagner
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Campanha implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5275371916730300510L;

    private Long id;
    private Long idTimeCoracao;
    private String nome;
    private LocalDate dtInicio;
    private LocalDate dtFim;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdTimeCoracao() {
		return idTimeCoracao;
	}
	public void setIdTimeCoracao(Long idTimeCoracao) {
		this.idTimeCoracao = idTimeCoracao;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public LocalDate getDtInicio() {
		return dtInicio;
	}
	public void setDtInicio(LocalDate dtInicio) {
		this.dtInicio = dtInicio;
	}
	public LocalDate getDtFim() {
		return dtFim;
	}
	public void setDtFim(LocalDate dtFim) {
		this.dtFim = dtFim;
	}

}
