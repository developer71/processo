/**
 * 
 */
package br.com.processo.sociotorcedor.resource;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import br.com.processo.sociotorcedor.client.Campanha;
import br.com.processo.sociotorcedor.entity.Socio;
import br.com.processo.sociotorcedor.exception.BusinessException;
import br.com.processo.sociotorcedor.exception.ExceptionResolver;
import br.com.processo.sociotorcedor.service.SocioService;
import br.com.processo.sociotorcedor.vo.SocioVO;

/**
 * @author Vagner
 *
 */
@RestController
@Api(value = "socios") //Diz ao Swagger que esse é um endpoint e REST deve ser documentado
@RequestMapping(value="/socios")
public class SocioResource {
	
	@Autowired
	private SocioService service;
	
	@Autowired
	private RestTemplate restTemplate;

	@ApiOperation(value="consulta todos os socios")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response=Socio.class ,message = "Pesquisa realizada com sucesso"),
			@ApiResponse(code = ExceptionResolver.CODE_SOCIO_NOT_FOUND, message = "Sem resultado")
	})

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Socio>> findAll() throws BusinessException {
        final List<Socio> socios = service.findAll();
        if (socios.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(socios, HttpStatus.OK);
    }

	@ApiOperation(value="consulta socio pelo Id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response=Socio.class ,message = "Pesquisa realizada com sucesso"),
			@ApiResponse(code = ExceptionResolver.CODE_SOCIO_NOT_FOUND, message = "Sem resultado")
	})
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Socio> findById(@PathVariable final Long id) throws BusinessException {
        final Optional<Socio> socioOptional = service.findOne(id);
        if (socioOptional.isPresent()) {
            return new ResponseEntity<>(socioOptional.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
	
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<List<Campanha>> create(@RequestBody final Socio socio, final UriComponentsBuilder ucBuilder) throws BusinessException {
        service.save(socio);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/socios/{id}").buildAndExpand(socio.getId()).toUri());
        final List<Campanha> campanhas = getCampanhasByIdTimeCoracao(socio.getIdTimeCoracao());
        return new ResponseEntity<>(campanhas, headers, HttpStatus.CREATED);
    }
    
    private List<Campanha> getCampanhasByIdTimeCoracao(Long idTimeCoracao){
    		final ResponseEntity<Campanha[]> responseEntity = restTemplate.getForEntity("http://CAMPANHA-SERVICE/campanhas" + "?idTimeCoracao={idTimeCoracao}", Campanha[].class, idTimeCoracao);
    		
    		return Arrays.asList(responseEntity.getBody());
    }
    
	
	@ApiOperation(value="Atualizar Socio")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response=SocioVO.class, message = "Socio atualizado com sucesso"),
			@ApiResponse(code = ExceptionResolver.CODE_SOCIO_NOT_INSERTED, message = "Socio não atualizado")
	})
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Socio> update(@PathVariable("id") final Long id, @RequestBody final Socio socio) throws BusinessException {
        final Optional<Socio> currentSocioTorcedor = service.findOne(id);
        if (currentSocioTorcedor.isPresent()) {
            final Socio socioExistente = currentSocioTorcedor.get();
            socioExistente.setNome(socio.getNome());

            service.save(socioExistente);

            return new ResponseEntity<>(socioExistente, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
	
	@ApiOperation(value="Excluir Socio")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Socio excluido com sucesso"),
			@ApiResponse(code = ExceptionResolver.CODE_SOCIO_NOT_INSERTED, message = "Socio não excluido")
	})
	@DeleteMapping(value = "/{id}")
    public ResponseEntity<Socio> delete(@PathVariable("id") final Long id) throws BusinessException {
        final Optional<Socio> socioAtual = service.findOne(id);
        if (socioAtual.isPresent()) {
            service.delete(socioAtual.get().getId());

            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
	
}
