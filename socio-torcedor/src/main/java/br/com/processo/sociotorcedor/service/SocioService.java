/**
 * 
 */
package br.com.processo.sociotorcedor.service;

import java.util.List;
import java.util.Optional;

import br.com.processo.sociotorcedor.entity.Socio;
import br.com.processo.sociotorcedor.exception.BusinessException;

/**
 * @author Vagner
 *
 */
public interface SocioService {
	
	/**
	 * 
	 * @return List<Socio>
	 * @throws BusinessException
	 */
    List<Socio> findAll() throws BusinessException;

 
    /**
     * 
     * @param id
     * @return Optional<Socio>
     * @throws BusinessException
     */
    Optional<Socio> findOne(final Long id) throws BusinessException;

    /**
     * 
     * @param socio
     * @return Optional<Socio>
     * @throws BusinessException
     */
    Optional<Socio> save(final Socio socio) throws BusinessException;


    /**
     * 
     * @param id
     * @throws BusinessException
     */
    void delete(final Long id) throws BusinessException;
	
	

}
