/**
 * 
 */
package br.com.processo.sociotorcedor.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.processo.sociotorcedor.entity.Socio;
import br.com.processo.sociotorcedor.exception.BusinessException;
import br.com.processo.sociotorcedor.repository.SocioRepository;

/**
 * @author Vagner
 *
 */
@Service
public class SocioServiceImpl implements SocioService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SocioRepository repository;

	@Override
	public List<Socio> findAll() throws BusinessException {
        try {
            return repository.findAll();
        } catch (final Exception e) {
            logger.error("Erro ao buscar Sócios.");
            throw new BusinessException(e.getMessage());
        }
	}

	@Override
	public Optional<Socio> findOne(Long id) throws BusinessException {
        try {
            return Optional.ofNullable(repository.findOne(id));
        } catch (final Exception e) {
            logger.error("Erro ao buscar Sócio Torcedor por ID.");
            throw new BusinessException(e.getMessage());
        }
	}

	@Override
	public Optional<Socio> save(Socio socio) throws BusinessException {
        try {
            final Optional<Socio> socioOpcional = repository.findByEmail(socio.getEmail());
            if (socioOpcional.isPresent()) {
                throw new BusinessException("Sócio Torcedor já existente pelo email informado.");
            }
            return Optional.of(repository.save(socio));
        } catch (final Exception e) {
            logger.error("Erro ao Inserir um Sócio Torcedor.");
            throw new BusinessException(e.getMessage());
        }
	}

	@Override
	public void delete(Long id) throws BusinessException {
        try {
            repository.delete(id);
        } catch (final Exception e) {
            logger.error(String.format("Erro ao Remover um Sócio Torcedor pelo ID: %s", id));
            throw new BusinessException(e.getMessage());
        }
		
	}

	
		
}
