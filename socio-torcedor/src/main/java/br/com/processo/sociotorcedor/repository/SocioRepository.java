package br.com.processo.sociotorcedor.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.processo.sociotorcedor.entity.Socio;

@Repository
public interface SocioRepository extends JpaRepository<Socio, Long> {
	
	Optional<Socio> findByEmail(final String email);
}
