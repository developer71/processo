/**
 * 
 */
package br.com.processo.sociotorcedor.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * @author Vagner
 *
 */
@JsonAutoDetect
@Entity
@Table(name="socio")
public class Socio implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 8101079117750901586L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name="IDTIMECORACAO")
    private Long idTimeCoracao;

    @NotNull
    private String nome;

    @Email
    @NotNull
    @Column(unique = true)
    private String email;

    @NotNull
    @Column(name="DATANASCIMENTO")
    private LocalDate dataNascimento;
	
	public Socio() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdTimeCoracao() {
		return idTimeCoracao;
	}

	public void setIdTimeCoracao(Long idTimeCoracao) {
		this.idTimeCoracao = idTimeCoracao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
}
