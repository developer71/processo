# Processo #

**Projeto de Estudo de MicroService**

Provê serviços REST para Gerenciar cadastro de socio-torcedor, no qual será utilizado para os servicos REST

Tecnologias

Esse projeto utiliza swagger, spring-boot, spring-data, spring-boot-actuator, zuul, 
eureka e Ribbon, como banco de dados em memória (h2).
Arquitetura :
	Desenvolvimento orientado a microservice.
	Arquitetura e desenvolvimento de APIs REST.
	Clean code.
	Boas práticas de programação utilizando Java 8
 	Foi projetado em 3 camadas (resource, service, repository). Todos os servicos estão cadastrados 
	no eureka que atua como um service registry, atraves do RibbonClient consigo efetuar 
	a comunicação e fazendo o balnaceamneto dos microservices que irei utilizar. 
	Zuul atua como porta de entrada para infraestrutura de servidores.
	
Obs: baixar os projetos Eureka, Zuul, Campanha e Socio-torcedor, Vogal: Crie quantas instancias desejar de cada service Campanha e Socio apenas mudando a porta

Inicie os seguintes comandos:

**java -jar zuul-proxy.jar**

**java -jar eureka-server.jar**

**java -jar campanha-0.0.1-SNAPSHOT.jar --port=????**

**java -jar socio-torcedor-0.0.1-SNAPSHOT.jar --port=????**


apos o inicio dos serviços acesse o seguinte link http://localhost:8761/

com esse link conseguimos ver todos os serviços que estão UP

para chamar os serviçoes acesse os seguintes links:

http://localhost:8089/socio-service/socios   -> Get
http://localhost:8089/socio-service/socios/1   -> Get
http://localhost:8089/socio-service/socios   -> Post
http://localhost:8089/socio-service/socios   -> Put
http://localhost:8089/socio-service/socios/1   -> Delete

http://localhost:8089/socio-service/socios/id/1

http://localhost:8089/campanha-service/campanhas -> GET
http://localhost:8089/campanha-service/campanhas/1 -> Get

http://localhost:8089/campanha-service/campanhas/ -> Post

http://localhost:8089/campanha-service/campanhas/1 ->Update

http://localhost:8089/campanha-service/campanhas/1 -> Delete


## Proximas implementações: ##
**- Adicionar o hystrix para qdo. acontecer um fallback, direcionar para um outro microservice.** 

**- Alterar o banco H2 para MongoDB**