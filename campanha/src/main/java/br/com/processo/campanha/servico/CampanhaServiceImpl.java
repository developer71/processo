/**
 * 
 */
package br.com.processo.campanha.servico;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.processo.campanha.entity.Campanha;
import br.com.processo.campanha.exception.BusinessException;
import br.com.processo.campanha.repository.CampanhaRepository;

/**
 * @author Vagner
 *
 */
@Service
public class CampanhaServiceImpl implements CampanhaService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final UnaryOperator<LocalDate> ADD_DAY = date -> date.plus(1, ChronoUnit.DAYS);

	@Autowired
	private CampanhaRepository repository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.processo.campanha.servico.CampanhaServico#findAll()
	 */
	@Override
	public List<Campanha> findAll() {
		return repository.findAll();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.processo.campanha.servico.CampanhaServico#findByNome(java.lang.String)
	 */
	@Override
	public Campanha findByNome(String nome) {
		return repository.findByNome(nome);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.processo.campanha.servico.CampanhaServico#atualizarCampanha(br.com.
	 * processo.campanha.entity.Campanha)
	 */
	@Override
	public Campanha atualizarCampanha(Campanha entity) {
		return repository.saveAndFlush(entity);
	}

	/**
	 * 
	 */
	@Override
	public List<Campanha> findByDtFimGreaterThanEqual(LocalDate dtNow) {
		DateTimeFormatter formatadorBarra = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String format = dtNow.format(formatadorBarra);

		return repository.findByDtFimGreaterThanEqual(LocalDate.parse(format));
	}

	@Override
	public List<Campanha> findByDtFimAfter(LocalDate dtNow) {

		return repository.findByDtFimAfter(dtNow);
	}


	@Override
	public List<Campanha> findAll(Long idTimeCoracao) {
		return repository.findByIdTimeCoracao(idTimeCoracao);
		//return repository.findByDtFimGreaterThanEqualAndIdTimeCoracao(LocalDate.now(), idTimeCoracao);
	}

	@Override
	public Optional<Campanha> findOne(Long id) {
		return Optional.ofNullable(repository.findOne(id));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.processo.campanha.servico.CampanhaServico#salvarCampanha(br.com.
	 * processo.campanha.entity.Campanha)
	 */
	@Override
	public Optional<Campanha> save(Campanha campanha) {

		Optional<Campanha> optional = null;

		try {
			if (campanha != null && campanha.getDtFim().isAfter(LocalDate.now().minusDays(1))) {
				List<Campanha> campanhas = repository.findByDtFimGreaterThanEqual(LocalDate.now().minusDays(1));
				List<Campanha> campanhasAlteradas = gerarListParaAtualizar(campanhas, campanha.getDtFim());
				atualizarListaCampanha(campanhasAlteradas);
				optional = Optional.of(repository.save(campanha));
			}
		} catch (final Exception e) {
			logger.error("Erro ao Salvar/Inserir uma Campanha.");
			throw new BusinessException(e.getMessage());
		}
		return optional;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.processo.campanha.servico.CampanhaServico#delete(java.lang.
	 * Long)
	 */
	@Override
	public void delete(Long id) {
		repository.delete(id);

	}

/**
 * 
 * @param campanhas
 * @return Map<LocalDate, Campanha>
 */
	private Map<LocalDate, Campanha> gerarMapPorData(List<Campanha> campanhas) {

		return campanhas.stream().collect(Collectors.toMap(Campanha::getDtFim, campanha -> campanha));

	}

	/**
	 * 
	 * @param campanhas
	 * @param dataNova
	 * @return List<Campanha>
	 */
	private List<Campanha> gerarListParaAtualizar(List<Campanha> campanhas, LocalDate dataNova) {
		List<Campanha> lst = new ArrayList<>();

		Map<LocalDate, Campanha> mapPorData = gerarMapPorData(campanhas);

		return validarDatasParaAtualizar(lst, mapPorData, dataNova);

	}

	/**
	 * 
	 * @param campanhas
	 * @param map
	 * @param data
	 * @return List<Campanha>
	 */
	private List<Campanha> validarDatasParaAtualizar(List<Campanha> campanhas, Map<LocalDate, Campanha> map,
			LocalDate data) {

		if (map.containsKey(data)) {
			Campanha campanha = map.get(data);
			campanhas.add(atualizarDataFinal(campanha));

			validarDatasParaAtualizar(campanhas, map, campanha.getDtFim());
		}
		return campanhas;

	}

	/**
	 * 
	 * @param lst
	 * @return
	 */
	private List<Campanha> atualizarListaCampanha(List<Campanha> lst) {
		return lst.stream().map(c -> repository.saveAndFlush(c)).collect(Collectors.toList());
	}

	/**
	 * 
	 * @param campanha
	 * @return Campanha
	 */
	private Campanha atualizarDataFinal(Campanha campanha) {

		campanha.setDtFim(ADD_DAY.apply(campanha.getDtFim()));
		return campanha;

	}
}
