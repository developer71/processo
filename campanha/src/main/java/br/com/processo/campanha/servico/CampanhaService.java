/**
 * 
 */
package br.com.processo.campanha.servico;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import br.com.processo.campanha.entity.Campanha;

/**
 * @author Vagner
 *
 */
public interface CampanhaService {
	
	/**
	 * 
	 * @return List<Campanha>
	 */
	List<Campanha> findAll();

	/**
	 * 
	 * @param idTimeCoracao
	 * @return List<Campanha>
	 */
	List<Campanha> findAll(final Long idTimeCoracao);
	
	/**
	 * 
	 * @param id
	 * @return Optional<Campanha>
	 */
	Optional<Campanha> findOne(final Long id);
	
	/**
	 * 
	 * @param campanha
	 * @return Optional<Campanha>
	 */
	Optional<Campanha> save(final Campanha campanha);
	
	/**
	 * 
	 * @param id
	 */
	void delete(final Long id);
	
	/**
	 * 
	 * @param nome
	 * @return
	 */
	Campanha findByNome(String nome);
	
	/**
	 * 
	 * @param entity
	 * @return
	 */
	Campanha atualizarCampanha(Campanha entity);
	
	/**
	 * 
	 * @param dtNow
	 * @return List<Campanha>
	 */
	List<Campanha> findByDtFimGreaterThanEqual(LocalDate dtNow);
	
	/**
	 * 
	 * @param dtNow
	 * @return List<Campanha>
	 */
	List<Campanha> findByDtFimAfter(LocalDate dtNow);
}
