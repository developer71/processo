/**
 * 
 */
package br.com.processo.campanha.resource;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import br.com.processo.campanha.entity.Campanha;
import br.com.processo.campanha.exception.ExceptionResolver;
import br.com.processo.campanha.servico.CampanhaService;


/**
 * @author Vagner
 *
 */
@RestController
@Api(value = "campanha") //Diz ao Swagger que esse é um endpoint e REST deve ser documentado
@RequestMapping(value="/campanhas")
public class CampanhaResource {
	
	@Autowired
	private CampanhaService service;
	
	@ApiOperation(value="consulta todos os Campanhas")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response=Campanha.class ,message = "Pesquisa realizada com sucesso"),
			@ApiResponse(code = ExceptionResolver.CODE_CAMPANHA_NOT_FOUND, message = "Sem resultado")
	})
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Campanha>> findAll()  {
        final List<Campanha> campanhas = service.findByDtFimGreaterThanEqual(LocalDate.now());
        if (campanhas.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(campanhas, HttpStatus.OK);
    }

	
	@ApiOperation(value="consulta todos os Campanhas pelo idTimeCoracao")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response=Campanha.class ,message = "Pesquisa realizada com sucesso"),
			@ApiResponse(code = ExceptionResolver.CODE_CAMPANHA_NOT_FOUND, message = "Sem resultado")
	})
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, params = "idTimeCoracao")
    public ResponseEntity<List<Campanha>> findAllByTimeCoracao(@RequestParam final Long idTimeCoracao) {
        final List<Campanha> campanhas = service.findAll(idTimeCoracao);
        if (campanhas.spliterator().getExactSizeIfKnown() <= 0) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(campanhas, HttpStatus.OK);
    }
	
	
	@ApiOperation(value="consulta todos os Campanhas pelo id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, response=Campanha.class ,message = "Pesquisa realizada com sucesso"),
			@ApiResponse(code = ExceptionResolver.CODE_CAMPANHA_NOT_FOUND, message = "Sem resultado")
	})
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Campanha> findById(@PathVariable final Long id)  {
        final Optional<Campanha> campanha = service.findOne(id);
        if (campanha.isPresent()) {
            return new ResponseEntity<>(campanha.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

	
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> create(@RequestBody final Campanha campanha, final UriComponentsBuilder ucBuilder)  {
        service.save(campanha);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/campanhas/{id}").buildAndExpand(campanha.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
	

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Campanha> update(@PathVariable("id") final Long id, @RequestBody final Campanha campanha) {
        final Optional<Campanha> currentCampanha = service.findOne(id);
        if (currentCampanha.isPresent()) {
            currentCampanha.get().setNome(campanha.getNome());

            service.save(currentCampanha.get());

            return new ResponseEntity<>(currentCampanha.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
	@ApiOperation(value="Delete Campanha")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Campanha excluido com sucesso"),
			@ApiResponse(code = ExceptionResolver.CODE_CAMPANHA_NOT_INSERTED, message = "Campanha não excluido")
	})
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Campanha> delete(@PathVariable("id") final Long id)  {
        final Optional<Campanha> currentCampanha = service.findOne(id);
        if (currentCampanha.isPresent()) {
            service.delete(currentCampanha.get().getId());

            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
