package br.com.processo.campanha.resource;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import br.com.processo.campanha.entity.Campanha;
import br.com.processo.campanha.servico.CampanhaService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class CampanhaResourceTest {
	
    @Autowired
    private MockMvc mvc;

    @MockBean
    private CampanhaService service;

	@Test
	public void deveRetornarCampanhasCadastradas() throws Exception{
        final List<Campanha> campanhas = new ArrayList<>();
        campanhas.add(criarCampanha().get());
        when(service.findByDtFimGreaterThanEqual(LocalDate.now())).thenReturn(campanhas);

        mvc.perform(MockMvcRequestBuilders.get("/campanhas")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
	}
	
    @Test
    public void deveRetornarNoContent() throws Exception {
        final List<Campanha> campanhas = new ArrayList<>();
        when(service.findByDtFimGreaterThanEqual(LocalDate.now())).thenReturn(campanhas);

        mvc.perform(MockMvcRequestBuilders.get("/campanhas")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }
    
    @Test
    public void deveRetornarNoContentNaoExistirCampanhasPeloIdTimeCoracao() throws Exception {
        final List<Campanha> campanhas = new ArrayList<>();
        when(service.findAll(1l)).thenReturn(campanhas);
        mvc.perform(MockMvcRequestBuilders.get("/campanhas?idTimeCoracao=1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deveRetornarCampanhasPeloIdTimeCoracaotest_deve_retornar_as_campanhas_cadastradas_pelo_id_time_coracao() throws Exception {
        final List<Campanha> campanhas = new ArrayList<>();
        campanhas.add(criarCampanha().get());
        when(service.findAll(1L)).thenReturn(campanhas);

        mvc.perform(MockMvcRequestBuilders.get("/campanhas?idTimeCoracao=1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void deveRetornarNoContentQuandoNaoExistirCampanhaPorId() throws Exception {
        final Long id = 0L;
        when(service.findOne(id)).thenReturn(Optional.ofNullable(null));

        mvc.perform(MockMvcRequestBuilders.get("/campanhas/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deveRetornarCampanhaPorId() throws Exception {
        final Long id = 1L;
        final Optional<Campanha> campanha = criarCampanha();
        when(service.findOne(id)).thenReturn(campanha);

        mvc.perform(MockMvcRequestBuilders.get("/campanhas/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void tdeveInserirCampanha() throws Exception {
        final Optional<Campanha> campanha = criarCampanha();
        when(service.save(campanha.get())).thenReturn(campanha);

        mvc.perform(MockMvcRequestBuilders.post("/campanhas")
                .content(new JSONObject("{'nome': 'teste'}").toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void deveRetornarBadRequestQuandoExecutarPostSemBody() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/campanhas")
                .content("")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deveAtualizarCampanha() throws Exception {
        final Optional<Campanha> campanha = criarCampanha();
        when(service.save(campanha.get())).thenReturn(campanha);
        when(service.findOne(1L)).thenReturn(campanha);

        mvc.perform(MockMvcRequestBuilders.put("/campanhas/1")
                .content(new JSONObject("{'nome': 'teste'}").toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void deveRetornarNotFoundQuandoNaoExistir() throws Exception {
        when(service.findOne(2L)).thenReturn(Optional.ofNullable(null));

        mvc.perform(MockMvcRequestBuilders.put("/campanhas/2")
                .content(new JSONObject("{'nome': 'teste'}").toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deveRetornarBadRequestQuandoExecutarPutSemBody() throws Exception {
        mvc.perform(MockMvcRequestBuilders.put("/campanhas/1")
                .content("")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deveRemoverCampanha() throws Exception {
        final Optional<Campanha> campanha = criarCampanha();
        when(service.findOne(1L)).thenReturn(campanha);

        mvc.perform(MockMvcRequestBuilders.delete("/campanhas/1"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deveRetornarNotFoundQuandoNaoExistirId() throws Exception {
        when(service.findOne(2L)).thenReturn(Optional.ofNullable(null));

        mvc.perform(MockMvcRequestBuilders.delete("/campanhas/2"))
                .andExpect(status().isNotFound());
    }
	
    private Optional<Campanha> criarCampanha() {
        
    	final Campanha campanha = new Campanha();
        campanha.setIdTimeCoracao(1L);
        campanha.setNome("Campanha1");
        campanha.setDtInicio(LocalDate.now());
        campanha.setDtFim(LocalDate.now().plusDays(1));
        return Optional.of(campanha);
    }
	

}
