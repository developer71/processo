package br.com.processo.campanha.servico;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.processo.campanha.entity.Campanha;
import br.com.processo.campanha.exception.BusinessException;
import br.com.processo.campanha.repository.CampanhaRepository;


//@RunWith(SpringRunner.class)
//@SpringBootTest
//@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@RunWith(MockitoJUnitRunner.class)
public class CampanhaServiceImplTest {

    @Mock
    private CampanhaRepository repository;
    

    private List<Campanha> criarListCampanha() {
        final Campanha campanha = new Campanha();
        campanha.setId(1l);
        campanha.setIdTimeCoracao(1L);
        campanha.setNome("Campanha1");
        campanha.setDtInicio(LocalDate.now());
        campanha.setDtFim(LocalDate.now().plusDays(1));
        final List<Campanha>lst = new ArrayList<>();
        lst.add(campanha);
        return lst;
    }
    
    @Test
    public void deveRetornarTodasCampanhasCorrentes()throws BusinessException{
    		final List<Campanha> list = criarListCampanha();
    				
    		when(repository.findAll()).thenReturn(list);
    		assertTrue(list.size() > 0);
    }
    
    @Test
    public void deveRetornarCampanhasPeloIdTimeCoracao()throws BusinessException{
		final List<Campanha> list = criarListCampanha();
		
		when(repository.findByDtFimGreaterThanEqualAndIdTimeCoracao(LocalDate.now(), 1l)).thenReturn(list);
		assertTrue(list.size() > 0);
    }
    
    @Test
    public void deveRetornarCampanhaPorId()throws BusinessException{
		final List<Campanha> list = criarListCampanha();
		
		when(repository.findOne(1l)).thenReturn(list.get(0));
		assertTrue(list.size() > 0);
    	
    }
    
    @Test
    public void deveSalvarCampanha()throws BusinessException{
		final List<Campanha> list = criarListCampanha();
		final Campanha campanha = list.get(0);
		when(repository.save(list.get(0))).thenReturn(list.get(0));
		assertEquals(campanha, list.get(0) );
    }
    
    @Test
    public void deveAtualizarCampanha()throws BusinessException{
		final List<Campanha> list = criarListCampanha();
		final Campanha campanha = list.get(0);
		
		when(repository.saveAndFlush(list.get(0))).thenReturn(list.get(0));
		assertEquals(list.get(0), campanha);
    }
}
